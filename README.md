# AstroGrep

A Windows File Searching Utility (grep).

Official page for AstroGrep is still http://astrogrep.sourceforge.net/

This Subversion import from https://sourceforge.net/projects/astrogrep/

## Source Code

Current version https://sourceforge.net/p/astrogrep/code/HEAD/tree/trunk/AstroGrep/

SourceForge is still the main code repository.

```
svn checkout https://svn.code.sf.net/p/astrogrep/code/trunk astrogrep-code
```

## Build

Run Docker container for build:

```powershell
Set-Location "path_to_source"
Get-ChildItem -Path ".\src\" -Recurse | Unblock-File
docker run --rm -i -t -v "${PWD}/src:C:\source" --name msbuilder mcr.microsoft.com/dotnet/framework/sdk
```

Необходим режим работы Docker "Window Container"

Click Docker Icon in System Tray In Context Menu → Click "Switch to Window/Linux Container"

Running **inside** a container:

```shell
cd source
nuget restore AstroGrep.sln
msbuild AstroGrep.sln /p:Configuration="Portable" /p:Platform="Any CPU" /p:TargetFrameworkVersion="v4.5" /t:Clean;Rebuild
```

Результат сборки можно найти в директории `.\WinformsGUI\bin\`

## Portable

Настройки AstroGrep Portable храняться в файлах:

+ AstroGrep.search.config
+ AstroGrep.general.config
+ AstroGrep.plugins.config
